module Polars


#WARN: obviously none of this is permanent
# still figuring out how I will call the wrapper

using Base: RefValue
using Arrow, Arrow.ArrowTypes
using Arrow: ArrowVector
using Arrow: validitybitmap
using Arrow: ValidityBitmap, Primitive, Offsets, List, BoolVector
using SentinelArrays: ChainedVector

using Libdl

const jl_polars_dir = joinpath(@__DIR__,"..","jl-polars")
const libpolars = joinpath(jl_polars_dir,"target","debug","libpolars.so")

const libhandle = Ref{Ptr{Cvoid}}()

function reloadlib()
    isassigned(libhandle) && dlclose(libhandle[])
    libhandle[] = dlopen(libpolars)
end

__init__() = reloadlib()

function cargobuild()
    cd(() -> run(`cargo build`), jl_polars_dir)
    reloadlib()
end

libptr(s::Symbol) = dlsym(libhandle[], s)


include("arrow.jl")


function rustmain()
    ptr = @ccall $(libptr(:putout))()::Ptr{Cvoid}
    @ccall $(libptr(:getback))(ptr::Ptr{Cvoid})::Cvoid
end

function test_arrow_import()
    arr = @ccall $(libptr(:arrow_array_export_test_2))()::RawArrowFFI
    sch = @ccall $(libptr(:arrow_field_export_test_2))()::RawSchemaFFI
    (arr, sch)
end

function test_arrow_export()
    v = Arrow.toarrowvector([1,missing,3])
    arr = RawArrowFFI(v)
    @ccall $(libptr(:arrow_import_test))(arr::RawArrowFFI)::Cvoid
    show(IOBuffer(), v)  # ensure v isn't GC'd for the time being...
end

function test_rustvec()
    x = [1,2,3]
    ptr = @ccall $(libptr(:vec_from_julia))(x::Ptr{Int64}, length(x)::Int64, sizeof(x)::Int64)::Ptr{Cvoid}
    @ccall $(libptr(:back_into_rust))(ptr::Ptr{Int64})::Cvoid
end

struct JlVec{T}
    ptr::Ptr{T}
    length::Int
    capacity::Int
end

JlVec(v::Vector{T}) where {T} = JlVec{T}(pointer(v), length(v), sizeof(v))

function jlvec_test()
    x = [1,2,3]
    jlx = JlVec(x)
    GC.@preserve x jlx @ccall $(libptr(:jlvec_test))(jlx::JlVec{Int64})::Cvoid
end


function arrow_mem_fuckage(v::AbstractVector{<:Union{Missing,Int}})
    arr = ArrowFFI(Arrow.toarrowvector(v))
    u = unsafe_wrap(Arrow.Primitive{eltype(v)}, arr)
    (arr, u)
end

#WARN:
# so far how I'm trying to do this is that whether the data is freed on the Julia or rust side
# depends on the arrays from which `chunks` is created
# If `chunks` owns its data, it will be GC'd normally by Julia
# If `chunks` refers to data owned by the ChunkedArray, it will be GC'd when the destructor
# for that object is called.
# Have to ensure you can't do both!

mutable struct ChunkedArray{T,A<:ArrowVector} <: AbstractVector{T}
    handle::Ptr{Cvoid}
    chunks::ChainedVector{T,A}
end

Base.size(ca::ChunkedArray) = size(ca.chunks)

Base.IndexStyle(::Type{<:ChunkedArray}) = Base.IndexLinear()
Base.getindex(ca::ChunkedArray, j) = getindex(ca.chunks, j)

function _rsChunkedArray_from_chunks(arrs::Vector{RawArrowFFI})
    ptr = pointer(arrs)
    GC.@preserve arrs begin
        @ccall $(libptr(:from_chunks))(ptr::Ptr{Cvoid}, length(arrs)::Int)::Ptr{Cvoid}
    end
end

function arrow_array_chunks(h::Ptr)
    arr = @ccall $(libptr(:arrow_array_chunks_arrays))(h::Ptr{Cvoid})::ArrowArrayRaw
    sch = @ccall $(libptr(:arrow_array_chunks_fields))(h::Ptr{Cvoid})::ArrowSchemaRaw
    (arr, sch)
    [Arrow.Primitive{Int64}(arr)]
end


#TODO: if you do it this way you are still going to have to pass the release to rust

#WARN: this is still trying to double free!

#TODO: this is going to be the simplest case of creating from a single Julia array which we will
# use as a template
function ChunkedArray(v::ArrowVector)
    GC.@preserve v begin
        arr = RawArrowFFI(v)
        h = _rsChunkedArray_from_chunks([arr])
        chs = arrow_array_chunks(h) |> ChainedVector
        ChunkedArray(h, chs)
    end
end
ChunkedArray(v::AbstractVector) = ChunkedArray(Arrow.toarrowvector(v))


end
