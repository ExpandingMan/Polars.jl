import Pkg; Pkg.activate(joinpath(@__DIR__,".."))
using Polars

using Polars: cargobuild
using Polars: ArrowFFI, SchemaFFI
using Polars: test_arrow_import, arrow_mem_fuckage

using Polars: ChunkedArray

using Arrow: Primitive, Offsets, List
