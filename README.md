# Polars

[![dev](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/Polars.jl/)
[![build](https://img.shields.io/gitlab/pipeline/ExpandingMan/Polars.jl/master?style=for-the-badge)](https://gitlab.com/ExpandingMan/Polars.jl/-/pipelines)

Julia wrapper for the [`polars`](https://www.pola.rs/) data frame library.

*This is an independent effort and in no way associated with the maintainers of `polars`.*

At the time of writing this project is extremely speculative, significant work has to be done on the
rust side to create a usable wrapper.

