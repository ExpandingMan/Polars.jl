use jlrs::prelude::*;
use std::ffi::c_void;
use polars::prelude::*;
use arrow2::array::{Array, PrimitiveArray, Utf8Array};
use arrow2::ffi;
use arrow2::datatypes::{Field, DataType};
use std::any::{Any, TypeId};
use core::mem;

use polars::chunked_array::ChunkedArray;

#[no_mangle]
pub unsafe extern "C" fn bad_array_pointer_demo() -> *mut c_void {
    let a: [i64; 4] = [1,2,3,4];
    //WARN: this re-allocates!!!
    let abox = Box::new(a);
    Box::leak(abox) as *mut _ as *mut _
}

#[no_mangle]
pub unsafe extern "C" fn array_pointer_demo() -> *mut c_void {
    let a: Vec<i64> = vec![1, 2, 30, 4];
    let abox = a.into_boxed_slice();
    Box::leak(abox) as *mut _ as *mut _
}

fn print_type_name<T>(_: T) {
    println!("type: {:?}", std::any::type_name::<T>());
}

fn print_ref_type_name<T: ?Sized>(_: &T) {
    println!("ref type: {:?}", std::any::type_name::<T>());
}

fn from_chunked_wtf() {
    let a: ChunkedArray<Int64Type> = ChunkedArray::from_vec("a", vec![1,2,3]);
    let v1 = a.data_views();
    //println!("wtf: {:?}", v1);
}

#[no_mangle]
pub unsafe extern "C" fn arrow_array_export(a: &dyn Array) -> ffi::ArrowArray {
    let abox = a.to_boxed();
    ffi::export_array_to_c(abox)
}

#[no_mangle]
pub unsafe extern "C" fn arrow_field_export(f: &Field) -> ffi::ArrowSchema {
    ffi::export_field_to_c(f)
}


#[no_mangle]
pub unsafe extern "C" fn arrow_array_export_test() -> ffi::ArrowArray {
    let a = PrimitiveArray::<i64>::from([Some(1), None, Some(3)]);
    arrow_array_export(&a as &dyn Array)
}

#[no_mangle]
pub unsafe extern "C" fn arrow_array_export_test_2() -> ffi::ArrowArray {
    let a = Utf8Array::<i32>::from([Some("kirk"), None, Some("spock")]);
    arrow_array_export(&a as &dyn Array)
}

#[no_mangle]
pub unsafe extern "C" fn arrow_field_export_test() -> ffi::ArrowSchema {
    let a = PrimitiveArray::<i64>::from([Some(1), None, Some(3)]);
    let f = Field::new("name", a.data_type().clone(), true);
    arrow_field_export(&f)
}

#[no_mangle]
pub unsafe extern "C" fn arrow_field_export_test_2() -> ffi::ArrowSchema {
    let a = Utf8Array::<i32>::from([Some("kirk"), None, Some("spock")]);
    let f = Field::new("name", a.data_type().clone(), true);
    arrow_field_export(&f)
}

#[no_mangle]
pub unsafe extern "C" fn arrow_import_test(arr: ffi::ArrowArray) {
    //let abox = ffi::import_array_from_c(arr, DataType::Int64).unwrap();
    //let a: &dyn Array = &(*abox);
    //let a = a.as_any().downcast_ref::<PrimitiveArray<i64>>().unwrap();
    println!("we got: {:?}", arr);
}

#[no_mangle]
pub unsafe extern "C" fn vec_from_julia(ptr: *mut i64, l: usize, cap: usize) -> *mut Vec<i64> {
    let mut a = Vec::from_raw_parts(ptr, l, cap);
    println!("got array from julia");
    to_jl_ptr(a)
}

#[no_mangle]
pub unsafe extern "C" fn back_into_rust(ptr: *mut Vec<i64>) {
    let a = mem::ManuallyDrop::new(from_jl_ptr(ptr));
    println!("got back: {:?}", *a);
}

#[repr(C)]
pub struct JlVec<T> {
    ptr: *mut T,
    length: usize,
    capacity: usize,
}

impl<T> JlVec<T> {
    unsafe fn to_vec(self) -> mem::ManuallyDrop<Vec<T>> { mem::ManuallyDrop::new(self.to_owned_vec()) }
    unsafe fn to_owned_vec(self) -> Vec<T> { Vec::from_raw_parts(self.ptr, self.length, self.capacity) }
}

//TODO: type?

#[no_mangle]
pub unsafe extern "C" fn jlvec_test(v: JlVec<i64>) {
    println!("fuck {:?}", v.length);
    let a = v.to_vec();
    //TODO: can we tell julia not to GC this?
    println!("what up {:?}", *a);
}



///////////////////////////////////////////////////////////////////////////////////
// the below is just a little closer to something we might actually use
//////////////////////////////////////////////////////////////////////////
unsafe fn to_jl_ptr<T>(x: T) -> *mut T {
    Box::into_raw(Box::new(x))
}

unsafe fn from_jl_ptr<T>(x: *mut T) -> T {
    *Box::from_raw(x)
}

#[no_mangle]
pub unsafe extern "C" fn make_test_series() -> *mut Series {
    let s = Series::new("a", [1,2,3]);
    to_jl_ptr(s)
}

#[no_mangle]
pub unsafe extern "C" fn series_func_test(sptr: *mut Series) {
    let s = from_jl_ptr(sptr);
    println!("{:?}", s)
}

//TODO: dealing with typing is going to be more complicated

#[no_mangle]
pub unsafe extern "C" fn from_chunks(arrsptr: *const ffi::ArrowArray, l: usize) -> *mut ChunkedArray<Int64Type> {
    //let arrs = std::slice::from_raw_parts(arrsptr, l);
    let mut cs: Vec<Box<dyn Array>> = Vec::with_capacity(l);
    for idx in 0..l {
        cs.push(ffi::import_array_from_c(arrsptr.add(idx).read(), DataType::Int64).unwrap());
    }
    let ca = ChunkedArray::from_chunks("name", cs);
    let ca = mem::ManuallyDrop::new(ca);
    to_jl_ptr(*ca)
}

#[no_mangle]
pub unsafe extern "C" fn arrow_array_chunks_arrays(captr: *mut ChunkedArray<Int64Type>) -> ffi::ArrowArray {
    let mut cs: &Vec<Box<dyn Array>> = captr.as_ref().unwrap().chunks();
    let mut cs = mem::ManuallyDrop::new(cs);
    let a = (*cs).first().unwrap();
    ffi::export_array_to_c((*a).to_boxed())
}

#[no_mangle]
pub unsafe extern "C" fn arrow_array_chunks_fields(captr: *mut ChunkedArray<Int64Type>) -> ffi::ArrowSchema {
    let mut caf = captr.as_ref().unwrap();
    let mut cadr = mem::ManuallyDrop::new(caf);
    let f = (*cadr).ref_field().to_arrow();
    ffi::export_field_to_c(&f)
}
