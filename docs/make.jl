using Polars
using Documenter

DocMeta.setdocmeta!(Polars, :DocTestSetup, :(using Polars); recursive=true)

makedocs(;
    modules=[Polars],
    authors="ExpandingMan <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/Polars.jl/blob/{commit}{path}#{line}",
    sitename="Polars.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/Polars.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
