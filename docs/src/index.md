```@meta
CurrentModule = Polars
```

# Polars

Documentation for [Polars](https://gitlab.com/ExpandingMan/Polars.jl).

```@index
```

```@autodocs
Modules = [Polars]
```
