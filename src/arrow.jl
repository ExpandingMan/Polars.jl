
# enventually this should be PR'd to Arrow.jl
# will put in FFI submodule

struct RawArrowFFI
    length::Clong
    null_count::Clong
    offset::Clong
    n_buffers::Clong
    n_children::Clong
    buffers::Ptr{Ptr{Cvoid}}
    children::Ptr{Ptr{RawArrowFFI}}
    dictionary::Ptr{RawArrowFFI}

    release::Ptr{Cvoid}
    private_data::Ptr{Cvoid}
end

struct ArrowFFI
    length::Int
    null_count::Int
    offset::Int
    # note that additional metadata is required to wrap the pointers which
    # is why they are still pointers here
    buffers::Vector{Ptr{UInt8}}
    children::Vector{ArrowFFI}
    #TODO: dictionary

    release::Ptr{Cvoid}

    # this field provided for cases where a reference to a buffer is needed
    # to keep from being GC'd
    ref::RefValue{Any}
end

function ArrowFFI(raw::RawArrowFFI; ref::Ref{Any}=Ref{Any}())
    # these are owned because they are only part of the interface object,
    # this does not take ownership of underlying data
    bufs = unsafe_wrap(Array, convert(Ptr{Ptr{UInt8}}, raw.buffers), raw.n_buffers; own=true)
    chptrs = unsafe_wrap(Array, raw.children, raw.n_children; own=true)
    ArrowFFI(raw.length,
             raw.null_count,
             raw.offset,
             bufs,
             map(ArrowFFI, chptrs),
             raw.release,
             ref,
            )
end

ArrowFFI(ptr::Ptr{RawArrowFFI}) = ArrowFFI(unsafe_load(ptr))

getbuffer(arr::ArrowFFI, j::Integer, ℓ::Integer; own::Bool=false) = unsafe_wrap(Array, arr.buffers[j], ℓ; own)

validitybitmap_bufsize(arr::ArrowFFI) = arr.null_count == 0 ? 0 : cld(arr.length, 8)

function get_validitybitmap_buffer(arr::ArrowFFI; own::Bool=false)
    arr.null_count == 0 && return UInt8[]
    if arr.buffers[1] == C_NULL
        UInt8[]
    else
        getbuffer(arr, 1, validitybitmap_bufsize(arr); own)
    end
end

function getbuffer(::Type{T}, arr::ArrowFFI, j::Integer; own::Bool=false) where {T}
    unsafe_wrap(Array, convert(Ptr{T}, arr.buffers[j]), arr.length; own)
end


struct RawSchemaFFI
    format::Ptr{Cchar}
    name::Ptr{Cchar}
    metadata::Ptr{Cchar}
    flags::Clong
    n_children::Clong
    children::Ptr{Ptr{RawSchemaFFI}}
    dictionary::Ptr{RawSchemaFFI}

    release::Ptr{Cvoid}
    private_data::Ptr{Cvoid}
end

struct SchemaFFI
    format::String
    name::String
    metadata::Dict{String,String}

    # these are obtained from the "flags" field
    flag_nullable::Bool
    flag_dictionary_ordered::Bool
    flag_map_keys::Bool

    children::Vector{SchemaFFI}
    #TODO: dictionary
    
    release::Ptr{Cvoid}
end

"""
    _schema_metadata_string

This is the algorithm for parsing the special arrow metadata string as described
[here](https://arrow.apache.org/docs/format/CDataInterface.html#c.ArrowSchema.metadata).
"""
function _schema_metadata_string(ptr::Ptr{Cchar})
    o = Dict{String,String}()
    ptr == C_NULL && return o

    npairs = unsafe_load(convert(Ptr{Int32}, ptr))
    ptr += 4
    sizehint!(o, npairs)

    for j ∈ 1:npairs
        nkb = unsafe_load(convert(Ptr{Int32}, ptr))
        ptr += 4
        k = unsafe_string(ptr, nkb)
        ptr += nkb
        
        nvb = unsafe_load(convert(Ptr{Int32}, ptr))
        ptr += 4
        v = unsafe_string(ptr, nvb)
        ptr += nvb

        o[k] = v
    end

    o
end

function SchemaFFI(raw::RawSchemaFFI)
    chptrs = unsafe_wrap(Array, raw.children, raw.n_children; own=true)
    chs = map(p -> SchemaFFI(p; own), chptrs)
    SchemaFFI(unsafe_string(raw.format),
              unsafe_string(raw.name),
              _schema_metadata_string(raw.metadata),
              raw.flags & 0x02 > 0,
              raw.flags & 0x01 > 0,
              raw.flags & 0x04 > 0,
              chs,
              raw.release,
             )
end

#TODO: nested stuff, List is fucked, for one

function Arrow.ValidityBitmap(arr::ArrowFFI; own::Bool=false)
    buf = get_validitybitmap_buffer(arr; own)
    ValidityBitmap(buf, 1, arr.length, arr.null_count)
end

function _check_nulls_type(::Type{T}, arr::ArrowFFI, name::AbstractString) where {T}
    if arr.null_count > 0 && !(T >: Missing)
        throw(ArgumentError("tried to create Arrow $name with missings but of non-missing eltype"))
    end
end

"""
    unsafe_wrap(T, arr::ArrowFFI; own=false) where {T<:ArrowVector}

Create an arrow array of the provided type from the `ArrowFFI` interface object.  If `own` the result
will take ownership of the underlying buffers.

This routine is unsafe because the memory that `arr` refers to may be freed.  If `own=false` this will
cause the output to be undefined.  If `own=true` this can lead to a double free.  To prevent this, the
underlying object must remain in scope, or set `own=true` if it is guaranteed that this will not
result in a double free.
"""
function Base.unsafe_wrap(::Type{<:Arrow.Primitive{T}}, arr::ArrowFFI; own::Bool=false) where {T}
    _check_nulls_type(T, arr, "Primitive")
    b = ValidityBitmap(arr; own)
    # for primitive this should always be last buffer
    data = getbuffer(Base.nonmissingtype(T), arr, length(arr.buffers); own)
    Primitive{T,typeof(data)}(UInt8[], b, data, arr.length, nothing)
end

function _buf_pointers(v::Arrow.Primitive, data)
    if Arrow.nullcount(v) == 0
        [pointer(v.data)]
    else
        [pointer(v.validity.bytes) + v.validity.pos - 1, pointer(data)]
    end
end

function ArrowFFI(v::Arrow.Primitive{T,Vector{T}}; ref::Ref{Any}=Ref{Any}(v)) where {T}
    ArrowFFI(length(v),
             Arrow.nullcount(v),
             0,
             _buf_pointers(v, v.data),
             ArrowFFI[],
             C_NULL,
             ref,
            )
end

